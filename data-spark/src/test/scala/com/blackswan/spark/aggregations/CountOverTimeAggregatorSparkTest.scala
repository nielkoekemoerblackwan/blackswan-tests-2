package com.blackswan.spark.aggregations

import java.sql.Timestamp
import java.time.ZonedDateTime

import com.holdenkarau.spark.testing.DataFrameSuiteBase
import org.apache.spark.sql.Row
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.types.{StructType, _}
import org.scalatest.{FunSuite, Matchers}

class CountOverTimeAggregatorSparkTest extends FunSuite with DataFrameSuiteBase with Matchers {

  override def conf = super.conf.set("spark.sql.session.timeZone", "UTC")

  private final val INPUT_SCHEMA = StructType(
    Seq(
      StructField("id", LongType, nullable = false),
      StructField("date", TimestampType, nullable = false)
    )
  )

  private final val OUTPUT_SCHEMA = StructType(
    Seq(
      StructField("id", LongType, nullable = true),
      StructField("sumOverTime", ArrayType(LongType, containsNull = false), nullable = true)
    ))


  test("test count over time") {

    val sourceDf = spark.read.schema(INPUT_SCHEMA).csv("src/test/resources/testData.csv")

    // TODO aggregate by id
    //val actualDf =

    val expectedDf = spark.createDataFrame(
      spark.sparkContext.parallelize(
        Seq(
          Row(1L, Seq[Long](1, 2, 3, 4, 5, 0, 0, 0, 0, 0, 0, 0)),
          Row(2L, Seq[Long](0, 0, 0, 0, 0, 1, 2, 2, 0, 1, 3, 1))
        )
      ),
      OUTPUT_SCHEMA
    )

    assertDataFrameEquals(expectedDf, actualDf)

  }
}
