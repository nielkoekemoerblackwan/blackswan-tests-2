# User Defined Aggregate Functions

Your task is to create a User Defined Aggregate Function (UDAF) which will count data over a time period.

Within the code, and tests, there are two TODO comments which indicate where code should be added.

### Input

You have been given a csv containing two unique ids, and a set of dates for each id indicating when an event occurred.

for example...

id|date
---|---
1|2020-01-01
1|2020-02-10
2|2020-01-10
2|2020-01-20
2|2020-02-01

### Output

Once the data has been aggregated the output should contain a row per id. 
Each row of data will contain the id, and a time series array. 
The time series array must be the same length for each row, and contain a value for every month in the input data.

for example...

id|value
---|---
1|[1,1]
2|[2,1]

### Alternative

If you are unable to complete the UDAF, can you write another form of aggregation that will produce the same result?

### Stretch

* Can you extend the UDAF to allow different counting periods. e.g. `Day`,`Week`, `Month`
* Can you extend the UDAF to allow `sum` over time, instead of `count`
* Can you extend the `sum` UDAF to allow summing of `DoubleType` as well as `LongType`


