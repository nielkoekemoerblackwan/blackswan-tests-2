name := "spark-test"

version := "0.1"

scalaVersion := "2.11.12"

libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.4.0" % "provided"
libraryDependencies += "com.holdenkarau" %% "spark-testing-base" % "2.4.0_0.12.0" % "test"

