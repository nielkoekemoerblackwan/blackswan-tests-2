
**The Test**

Build the responsive design from the designs found in the "bs-bud-sitebuild" folder.
For the images use any placeholders you want

- Please use vanilla CSS without preprocessors
- Please don't use any JS framework
- The main evaulation points: responsivity, clean and readable html and css, structure, mobile first
- Not required: Animations
- Bonus: Git history